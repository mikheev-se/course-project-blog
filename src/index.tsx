import ReactDOM from 'react-dom/client';
import './index.css';
import { App } from './app/';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { store } from './app/store';
import { fetchUsers } from './components/User/usersSlice';
import { fetchPosts } from './components/Post/postsSlice';
import { fetchComments } from './components/Comment/commentsSlice';

store.dispatch(fetchUsers());
store.dispatch(fetchPosts());
store.dispatch(fetchComments());

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
