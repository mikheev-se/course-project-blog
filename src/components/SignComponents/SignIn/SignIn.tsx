import React from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../app/store';
import { signInDto } from '../../../types/types';
import { signIn } from '../../User/usersSlice';
import '../Sign.css';

const toggleClass = (elem: HTMLElement, className: string) => {
  elem.className = elem.className.includes(className)
    ? elem.className.replace(className, '')
    : elem.className.concat(' ' + className);
};

export function SignIn() {
  const dispatch = useDispatch<AppDispatch>();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as signInDto;

    const result = await dispatch(signIn(formProps)).unwrap();

    if (result.statusCode && result.statusCode === 403) {
      const div = document.getElementById('error-messages') as HTMLElement;
      const username = document.getElementById('username') as HTMLInputElement;
      const password = document.getElementById('password') as HTMLInputElement;

      let target: HTMLInputElement;

      if (div) {
        div.innerText = result.message;
        if (result.message.includes('имя')) {
          target = username;
        } else {
          target = password;
        }

        toggleClass(target, 'error');

        setTimeout(() => {
          div.innerText = '';
          toggleClass(target, 'error');
        }, 3000);
      }
    } else {
      localStorage.setItem('jwtToken', result.jwtToken);
      localStorage.setItem('userId', result.user.userId.toString());
      localStorage.setItem('username', result.user.username);
      window.location.assign('/');
    }
  };

  return (
    <div className='Sign'>
      <form className='input-form' onSubmit={handleSubmit} target='/'>
        <label className='required'>Имя пользователя</label>
        <input
          placeholder='Имя пользователя'
          name='username'
          id='username'
          maxLength={30}
          required
        />
        <label className='required'>Пароль</label>
        <input
          placeholder='Пароль'
          type='password'
          name='password'
          id='password'
          maxLength={30}
          required
        />
        <input type='submit' className='submit' value={'Войти!'} />
      </form>
      <div id='error-messages'></div>
    </div>
  );
}
