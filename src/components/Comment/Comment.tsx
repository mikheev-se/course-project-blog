import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../app/store';
import { CommentEntity } from '../../types/types';
import { selectUserById } from '../User/usersSlice';
import './Comment.css';
import { deleteComment } from './commentsSlice';

type Props = {
  comment: CommentEntity;
};

export function Comment({ comment }: Props) {
  const author = useSelector((state: RootState) =>
    selectUserById(state, comment.authorId)
  );

  const dispatch = useDispatch<AppDispatch>();

  if (!author) {
    return <div>Непредвиденная ошибка: у комментария нет автора...</div>;
  }

  const handleDelete = async (commentId: number) => {
    await dispatch(deleteComment(commentId)).unwrap();
  };

  return (
    <div className='Comment'>
      <div className='header'>
        <div>
          <a href={`/users/${author.userId}`} className='username'>
            {author.username}
          </a>
          {' // '}
          <span className='date'>
            {new Date(comment.createdAt).toLocaleString('ru')}
          </span>
        </div>
        {comment.authorId.toString() === localStorage.getItem('userId') ? (
          <div className='comment-change'>
            <a
              href='#'
              onClick={async () => {
                await handleDelete(comment.commentId);
              }}
            >
              Удалить
            </a>
          </div>
        ) : (
          ''
        )}
      </div>
      <div className='content'>
        {comment.content}
        {comment.createdAt === comment.updatedAt ? (
          ''
        ) : (
          <div className='date'>
            Изменено: {new Date(comment.updatedAt).toLocaleString('ru')}
          </div>
        )}
      </div>
    </div>
  );
}
