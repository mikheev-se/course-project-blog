import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { AppDispatch, RootState } from '../../app/store';
import { Comment } from '../Comment/Comment';
import './CommentsList.css';
import { addNewComment, selectPostComments } from '../Comment/commentsSlice';

export function CommentsList() {
  const { id } = useParams();
  const postId = parseInt(id as string);

  const comments = useSelector((state: RootState) =>
    selectPostComments(state, postId)
  );

  const dispatch = useDispatch<AppDispatch>();

  const handleCreateComment = async (
    event: React.FormEvent<HTMLFormElement>,
    postId: number
  ) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as { content: string };

    const button = document.getElementById('input-submit') as HTMLInputElement;
    const originalValue = button.value;

    const textarea = document.getElementById(
      'input-comment'
    ) as HTMLTextAreaElement;

    if (!textarea.value) {
      return;
    }

    try {
      await dispatch(
        addNewComment({ postId: postId, content: formProps.content })
      ).unwrap();
      button.value = 'Отправлено!';
      setTimeout(() => (button.value = originalValue), 3000);

      textarea.value = '';
    } catch (err) {
      button.value = 'Авторизуйтесь, чтобы оставить комментарий!';
      setTimeout(() => (button.value = originalValue), 3000);
    }
  };

  return (
    <div>
      {comments.length > 0 ? (
        <div className='CommentsList'>
          <h3>Ответы:</h3>
          {comments.map((comment) => (
            <Comment comment={comment} key={comment.commentId} />
          ))}
        </div>
      ) : null}

      <div className='create-comment'>
        <form
          onSubmit={async (event) => {
            await handleCreateComment(event, postId);
          }}
        >
          <textarea
            wrap='soft'
            id='input-comment'
            className='input-comment'
            name='content'
            placeholder='Есть что сказать? Оставьте комментарий!!'
          />
          <input
            type='submit'
            className='input-submit'
            id='input-submit'
            value='Отправить'
          />
        </form>
      </div>
    </div>
  );
}
