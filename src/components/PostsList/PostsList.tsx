import { useSelector } from 'react-redux';
import { AppRoutes } from '../../api';
import { RootState, store } from '../../app/store';
import { PostEntity } from '../../types/types';
import { selectAllPostsSortedById } from '../Post/postsSlice';
import './PostsList.css';

const renderPosts = (posts: PostEntity[]) =>
  posts.map((post) => (
    <a
      href={AppRoutes.POSTS_BASE + '/' + post.postId.toString()}
      key={post.postId}
    >
      <div className='Post'>
        <span className='id'>#{post.postId}</span>
        <span className='id'>#{post.authorId}</span>
        <span>
          {post.content.length > 20 + 3
            ? post.content.split(' ').slice(0, 20).join(' ') + '...'
            : post.content}
        </span>
        <span className='date'>
          {new Date(post.createdAt).toLocaleDateString('ru')}
        </span>
        <span className='date'>
          {new Date(post.updatedAt).toLocaleDateString('ru')}
        </span>
      </div>
    </a>
  ));

export function PostsList() {
  const posts = useSelector((state: RootState) =>
    selectAllPostsSortedById(state)
  );

  if (posts.length === 0) {
    return <div className='loading'>Loading...</div>;
  }

  return (
    <div>
      <h2>Список постов</h2>
      <div className='PostsList'>
        <div className='header'>
          <span className='id'>PostID</span>
          <span className='id'>AuthorID</span>
          <span>Содержание поста</span>
          <span className='date'>Дата создания</span>
          <span className='date'>Дата изменения</span>
        </div>
        {renderPosts(posts)}
      </div>
    </div>
  );
}
