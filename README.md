# Приложения для ведения блога

Данное веб-приложения предоставлять функционал для ведения собственного блога в интернете. Как (авторизованный) пользователь я могу:

- Создавать посты
- Комментировать свои посты и посты других пользователей
- Просматривать профили других пользователей сервиса
- Удалить свой профиль, посты и комментарии
- Изменить свой профиль и посты
